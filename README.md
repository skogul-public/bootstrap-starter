# bootstrap-starter

Starter project for a [Bootstrap](https://getbootstrap.com/)-based website. `make` targets are included to make development, containerization, and releasing easier.

## Prerequisites

- [Docker](https://www.docker.com/) - Used to run development server and for packaging final artifact
- [make](https://www.gnu.org/software/make/) - Convenient for running common scripts

## Develop

Execute the following command to start a docker `nginx` container hosting the under development website.

```bash
make run
```

The website will be available at http://localhost:8080. Changes may be loaded by refreshing the browser; the 
container does not need to be restarted for changes to take effect. The container may be stopped by running `make stop`.

## Build & Release

To package the website into a docker image, run `make build`. This copies all files in the `web/` directory onto a `nginx` image. The website and webserver are bundled together into a single docker image for distribution.

To run the built image locally, run `make run_release`. To push the image, run `make push`.


## More Info

The template for this website was taken from [Bootstrap Examples](https://getbootstrap.com/docs/4.0/examples/). A different start template can be swapped out by copying it into the `web/` directory.
