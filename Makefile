NAME=bootstrap-starter
VERSION=0.0.1
DOCKER_REPO=hub.docker.com
DOCKER_GROUP=$(USER)
DOCKER_IMAGE=$(DOCKER_REPO)/$(DOCKER_GROUP)/$(NAME):$(VERSION)

ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

run:
	@docker run --name $(NAME) -p 8080:80 -v $(ROOT_DIR)/web:/usr/share/nginx/html:ro -d nginx
	@echo 'container $(ANME) running at http://localhost:8080. use "make stop" to stop container.'

stop:
	@docker stop $(NAME) > /dev/null && docker rm $(NAME) > /dev/null
	@echo 'stopped and removed container $(NAME)'

build:
	@docker build -t $(DOCKER_IMAGE) .

run_release:
	@docker run -p 8080:80 $(DOCKER_IMAGE)

release:
	@docker push $(DOCKER_IMAGE)